# Logistics regression
Fit de uma transição discreta com ruído

Seja a transição modelada por
`f(t) = e^g(t)/(e^g(t) + 1) = 1/(1 + e^(-g(t)), g(t) = a*(t-t0)`

O objecto será minimizar a diferença entre a soma das diferenças quadráticas entre a função e os dados obtidos.

Se o erro for `E = 1/2 * sum(i, (f(t_i) - y(i))^2))`

Logo
```
dE/da = sum(i, (f(t_i) - y(i)) * df/da )
      = sum(i, (f(t_i) - y(i)) * df/dg * dg/da )`

df/dg = exp(-g(t)) / (1 + exp(-g(t)))^2
      = exp(-g(t)) * f(t)^2

dg/da = (t-t0)
```
=>
```
dE/da = sum(i, (f(t-i) - y(i)) * exp(-g(t_i)) * f(t_i)^2 * (t_i-t0) )
```
Para t0:
```
dE/da = sum(i, (f(t_i)-y(i)) * df/dg * dg/dt0 )`

dg/dt0 = -a
```
=>

```
dE/dt0 = sum(i, (f(t_i) - y(i)) * exp(-g(t_i)) * f(t_i)^2 * (-a) )`

```

## Reflexão sobre a transição
A escolha da função que modela a transição é um bocado ad-hoc.
Portanto, qual será o mecanismo por trás da transição?
Será que se pode modelar um fenómeno random, tal que a probabilidade de transição é a cumulativa de uma binomial?
