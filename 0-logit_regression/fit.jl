using DelimitedFiles

printVecHor(v) = println(join([string(x) for x in v], '\t'))

function fit(x,y)
	alpha = 0.5

	x0 = sum(x) / length(x)
	a  = 1.0

	for i=1:100
		fi  = 1.0 ./ (1.0 .+ exp.(a .* (x .- x0)))
		err = fi .- y
		sumerr2 = sum(err.^2)

		dedx  = err .* exp.(a.*(x.-x0)) .* fi.^2.0
		deda  = sum(dedx .* (-x .+ x0))
		dedx0 = sum(dedx .* a)

		a  -= alpha * deda
		x0 -= alpha * dedx0
		
		if i%10 == 1
			println("$i\t$sumerr2\t$a\t$x0")
		end
	end
end


function fit1(x,y)
	alpha = 0.1

	a  = 1.0
	x0 = sum(x) / length(x)
	
	th0 = -x0
	th1 = 1.0

	for k=1:100
		#fk  = 1.0 ./ (1.0 .+ exp.(th0 .+ th1 .* x))
		fk  = 1.0 ./ (1.0 .+ exp.(a .* (x .- x0)))

		err = fk .- y
		sumerr2 = sum(err.^2)

		#dLdth0 = sum(err)
		#dLdth1 = sum(err .* x)
		dLda  = sum(err .* (x .- x0))
		dLdx0 = sum(err .* (-a))

		a  += alpha * dLda
		x0 += alpha * dLdx0

		#a  = th1
		#t0 = -th0/th1
		
		if k%10 == 1
			#println("$k\t$sumerr2\t$th0\t$th1")
			println("$k\t$sumerr2\t$a\t$x0")
		end
	end
end


function fitBrute(x,y)
	alpha = 10^2.0

	dt = 1e-6

	a  = -1.0
	x0 = sum(x) / length(x)

	ra = rand()
	rx0 = rand()
	a  = -1.6*(1-ra) -0.9*ra
	x0 = 2.55*(1-rx0) + 3.0*rx0
	
	th0 = -x0
	th1 = 1.0

	f(a,x0,x) = 1.0 ./ (1.0 .+ exp.(a .* (x .- x0)))
	
	N = 50
	alist=zeros(N+1)
	x0list=zeros(N+1)

	for k=1:N
		alist[k] = a
		x0list[k] = x0

		#fk  = 1.0 ./ (1.0 .+ exp.(th0 .+ th1 .* x))
		fk   = f(a,x0,x)
		fak  = f(a+dt,x0,x)
		fx0k = f(a,x0+dt,x)
		
		pfk    = prod(fk.^y   .* (1.0 .- fk).^(1.0.-y))
		pfak   = prod(fak.^y  .* (1.0 .- fak).^(1.0.-y))
		pfx0k  = prod(fx0k.^y .* (1.0 .- fx0k).^(1.0.-y))

		dfda  = (pfak  - pfk) / dt
		dfdx0 = (pfx0k - pfk) / dt
	
		#println()
		#println([a x0])
		#println([dfda dfdx0])
		printVecHor([a,x0])

		a  += dfda * alpha
		x0 += dfdx0 * alpha

	end
	alist[N+1] = a
	x0list[N+1] = x0

	return [alist x0list]
end


function fitBrute1(x,y)
	alpha = 0.1

	dt = 1e-6

	a  = -1.0
	x0 = sum(x) / length(x)
	
	th0 = -x0
	th1 = 1.0

	f(a,x0,x) = 1.0 ./ (1.0 .+ exp.(a .* (x .- x0)))
	
	N = 20

	for k=1:N
		
		#fk  = 1.0 ./ (1.0 .+ exp.(th0 .+ th1 .* x))
		fk   = f(a,x0,x)
		fak  = f(a+dt,x0,x)
		fx0k = f(a,x0+dt,x)
		
		pfk    = sum(y.*log.(fk)   .+ (1 .- y).*log.(1.0 .- fk))
		pfak   = sum(y.*log.(fak)  .+ (1 .- y).*log.(1.0 .- fak))
		pfx0k  = sum(y.*log.(fx0k) .+ (1 .- y).*log.(1.0 .- fx0k))

		dfda  = (pfak  - pfk) / dt
		dfdx0 = (pfx0k - pfk) / dt
	
		#println()
		println([a x0])

		a  += dfda * alpha
		x0 += dfdx0 * alpha

	end
end

data = readdlm("study.tsv")

x = data[:,1]
y = data[:,2]

traj = fitBrute(x,y)
