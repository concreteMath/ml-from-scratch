using LinearAlgebra

function fakedata(nGauss, nPoints)
	points = zeros(nPoints*nGauss, 3)
	for i=1:nGauss
		m = 8*(rand(2)*2 .- 1)
		sigma = rand(2) .+ 0.2
		t = rand()*2*pi
		mRot = [cos(t) -sin(t); sin(t) cos(t)]
		for j=1:nPoints
			k = (i-1) * nPoints + j
			uv = randn(2) .* sigma
			xy = m + mRot*uv
			points[k,1:2] = xy
			points[k,3] = i
		end
	end
	return points
end


function cov(x,y)
	x1 = x .- sum(x)/length(x)
	y1 = y .- sum(y)/length(y)
	return sum(x1.* y1)/length(x)
end


function classificacao(nGauss, training, samples)
	nTrain = size(training, 1)

	# Training
	mu = zeros(nGauss, 2)
	sg = zeros(nGauss, 2, 2)
	
	for g=1:nGauss
		points = training[training[:,3] .== g, 1:2]
		mu[g,:] = sum(points, dims=1) ./ size(points, 1)
		for i=1:2
			for j=1:2
				sg[g,i,j] = cov(points[:,i], points[:,j])
			end
		end
		#println()
		#println(mu[g,:])
		#println(sg[g,:,:])
	end

	# Cálculo da matriz inversa da covariância
	sginv = zeros(nGauss, 2, 2)
	for g=1:nGauss
		sginv[g,:,:] = inv(sg[g,:,:])
	end

	# Classification
	nSamp = size(samples,1)
	class = zeros(nSamp)
	for k=1:nSamp
		score = zeros(nGauss)
		for g=1:nGauss
			v        = samples[k,:] - mu[g,:]
			# Falta o det
			score[g] = - v' * sginv[g,:,:] * v - 0.5*det(sg[g,:,:])
		end
		# O 0.3 serve para dar uma cor ligeiramente diference da origem
		class[k] = findmax(score)[2] + 0.3
	end

	return class
	
end

function sim(nGauss, nSamples)
	samples = (rand(nSamples,2)*2 .- 1)*8
	data = fakedata(nGauss, 1000)
	class = classificacao(nGauss, data, samples)
	return [[samples class]; data]
end
