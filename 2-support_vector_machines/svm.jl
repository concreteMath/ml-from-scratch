function fakedata(nGauss, nPoints)
	points = zeros(nPoints*nGauss, 2)
	c      = zeros(nPoints*nGauss)
	for i=1:nGauss
		m = 8*(rand(2)*2 .- 1)
		sigma = rand(2) .+ 0.2
		t = rand()*2*pi
		mRot = [cos(t) -sin(t); sin(t) cos(t)]
		for j=1:nPoints
			k = (i-1) * nPoints + j
			uv = randn(2) .* sigma
			xy = m + mRot*uv
			points[k,:] = xy
			c[k] = i
		end
	end
	return points, c
end


function svm(x, y)
	lambda = 16
	alpha  = 0.02
	nPoints, nDim = size(x)
	w = ones(nDim)
	b = 1
	dEdw = zeros(nDim)
	dEdb = 0.0
	for k=1:1000
		for i=1:nPoints
			z = 1 - y[i] * (x[i,:]'*w - b)

			# Derivada do max
			dEdmax = (z > 0) * 1.0

			# Alternativa aproximada
			#dEdmax = (1 + z/sqrt(z^2 + 0.1))/2

			for j=1:nDim
				dEdw[j] += dEdmax * (-y[i]*x[i,j])
			end

			dEdb += dEdmax * y[i]
		end
		dEdw = lambda .* dEdw./nPoints + w
		dEdb = lambda * dEdb/nPoints

		w -= alpha .* dEdw
		b -= alpha  * dEdb
		println("$w\t$b")
	end
	return w, b
end

x,y = fakedata(2, 10)
# normalização das classes
y = y .* 2 .- 3
w, b = svm(x,y)

# Bounding box para o plot
x1min = minimum(x[:,1], dims=1)[1]
x1max = maximum(x[:,1], dims=1)[1]
# Pontos x1 dos extremos de x2
x2min = minimum(x[:,2], dims=1)[1]
x2max = maximum(x[:,2], dims=1)[1]
x2minx1 = (-x2min*w[2] + b)/w[1]
x2maxx1 = (-x2max*w[2] + b)/w[1]
# Redefinição dos extremos de x1 considerando os x1 dos extremos de x2
x1min = max(x1min, min(x2minx1, x2maxx1))
x1max = min(x1max, max(x2minx1, x2maxx1))

t = collect(0:0.1:1)
plotx1 = ones(length(t)) .* x1min + t .* (x1max-x1min)
plotx2 = (-plotx1.*w[1] .+ b) ./ w[2]
plotpoints = [plotx1 plotx2]

# Output
writedlm("/tmp/plotdata", [x y])
writedlm("/tmp/plotdata1", plotpoints)

