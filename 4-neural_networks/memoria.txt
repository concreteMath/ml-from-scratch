2020-07-21

Ontem finalmente consegui implementar uma rede neuronal.
Verifiquei que quantos menos inputs de treino tem, mais fácilmente converge.
Penso que seja um problema de overfitting.

Dado que queria reproduzir a função x->x+10, decidi reduzir a rede ao essencial, um neuronio de input, um escondido e um de output.
Provavelmente até poderia eliminar o de output de deixar como parâmetros de optimização apenas o peso entre o neurão de input e o escondido e a bias do escondido.

A minha ideia é a seguinte:
- Dada a simplicidade da função e da rede, se convergir para um estado de treino, então a rede deverá conseguir calcular para todos os outros.
