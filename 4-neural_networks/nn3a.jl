# Adicionei monte-carlo sampling

# 2020-07-22


function showtype(x)
	#println(typeof(x))
	#println(size(x))
end


function nn_create_and_train(nE)
	data_out = []


	# r=0.005 quase que garante a convergência da rede entre 2000 e 3000 passos.
	# O treino é feito com números no intervalo 1:10
	r = 0.0001

	mcSamples = 1

	# pesos
	w = [randn(nE,1), randn(1,nE)]
	b = [randn(nE,1), randn(1,1)]

	# resultados intermedios
	net = [zeros(nE,1), zeros(1,1)]

	for i=1:100000
		#x = rand()*10
		x = rand(0:7)
		
		# facil de treinar
		y = x + 10
		# dificil de treinar
		#y = x^2
		# quase impossivel de treinar com poucos neuroes
		#y = x%2

		dW = [zeros(nE,1), zeros(1,nE)]
		dB = [zeros(nE,1), zeros(1,1)]

		for j=1:mcSamples

			output = ones(1) * x
			for i=1:2
				net[i][:] = w[i] * output .+ b[i]
				output = max.(0, net[i])
			end

			err = output .- y

			push!(data_out, [i y output[1]])

			# BACKPROPAGATION
			back = [i==j for i=1:1 for j=1:1] .* 1.0
			for i=2:-1:1

				println("err: ", err)
				println("back: ", back)
				println("net_i:", net[i])
				
				# alteração dos pesos da camada actual
				back_i = err * back * (net[i] .> 0)
				if i>1
					dW[i] += back_i .* max.(0, net[i-1])
				else
					dW[i] += back_i .* x
				end
				dB[i] += back_i

				# actualização da backpropagation para os restantes layers
				back = back * ((net[i] .> 0) .* w[i])

			end
			
		end

		w[1] -= r.* dW[1] ./ mcSamples
		w[2] -= r.* dW[2]./ mcSamples
		b[1] -= r.* dB[1] ./ mcSamples
		b[2] -= r.* dB[2] ./ mcSamples

		#println("----------------")

	end

	for i=1:2
		println(stderr, w[i], "\t", b[i])
	end

	return data_out
end


data_out = nn_create_and_train(16)
