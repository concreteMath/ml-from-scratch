# Adicionei monte-carlo sampling


function showtype(x)
	#println(typeof(x))
	#println(size(x))
end


function nn_create_and_train(nE)
	data_out = []


	# r=0.005 quase que garante a convergência da rede entre 2000 e 3000 passos.
	# O treino é feito com números no intervalo 1:10
	r = 0.0001

	mcSamples = 1

	# pesos
	w = [randn(nE,1), randn(1,nE)]
	b = [randn(nE), randn(1)]

	# resultados intermedios
	net = [zeros(nE,1), zeros(1,1)]

	for i=1:100000
		#x = rand()*10
		x = rand(0:7)
		
		# facil de treinar
		#y = x + 10
		# dificil de treinar
		#y = x^2
		# quase impossivel de treinar com poucos neuroes
		y = x%2

		dW22 = zeros(1,nE)
		dW21 = zeros(nE,1)
		dB22 = zeros(1)
		dB21 = zeros(nE)

		for j=1:mcSamples

			output = ones(1) * x
			#println("outp:", output)
			for i=1:2
				#println("   i:", i)
				#println("  wi:", w[i])
				#println("  bi:", b[i])
				net[i][:] = w[i] * output .+ b[i]
				output = max.(0, net[i])
				#println("neti:",net[i])
				#println("outp:", output)
			end

			#println()

			err = output .- y
			#println(i, "\t", y, "\t", output[1])
		#println(i, "\t", y, "\t", output[1])
		push!(data_out, [i y output[1]])

		#println(" err:", err)

		dW22 += (max.(0, net[1]) * err)'
		#println("dW22:", dW22)

		dW11 = x .* (net[1] .> 0)
		#println("dW11:", dW11)
		dW21 += (w[2]' .* dW11) * err
		#println("dW21:", dW21)

		dB22 += err[:]
		#println("dB22:", dB22)

		dB11 = 1.0 .* (net[1] .> 0)
		#println("dB11:", dB11)
		dB21 += (w[2]' .* dB11) * err[:]
		#println("dB21:", dB21)

	end

	w[2] -= r.* dW22 ./ mcSamples
	w[1] -= r.* dW21 ./ mcSamples
	b[2] -= r.* dB22 ./ mcSamples
	b[1] -= r.* dB21 ./ mcSamples

	#println("----------------")

end

for i=1:2
	println(stderr, w[i], "\t", b[i])
end

return data_out
end


data_out = nn_create_and_train(16)
