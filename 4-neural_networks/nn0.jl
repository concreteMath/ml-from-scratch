function showtype(x)
	println(typeof(x))
	println(size(x))
end


function nn_create_and_train(nE)
	r = 0.5

	# pesos
	w = [randn(nE,1), randn(1,nE)]
	b = [randn(nE), randn(1)]

	# resultados intermedios
	net = [zeros(nE,1), zeros(1,1)]

	for i=1:1000
		x = rand(1:10)
		#y = x^2
		y = x

		output = x
		for i=1:2
			net[i] = w[i] * output.+ b[i]
			output = max.(0, net[i])
		end
		
		println(x)
		println(net[1])
		println(net[2])

		err = output .- y

		# Training
		dW11 = x * (net[1] .> 0)
		dW21 = (w[2]*dW11) .* (net[2] .> 0) .* ones(2)

		dW22 = max.(0, net[1]) * (net[2] .> 0)'
		dW22 .* err

		dB11 = (net[1] .> 0) .* 1.0
		dB21 = ((w[2]*dB11 ) .* (net[2] .> 0))[1] .* ones(2)

		dB22 = (net[2] .> 0) .* 1.0
		dB22 *= err
		dB22 = dB22[:]


		w[1] -= r.* dW21
		w[2] -= r.* permutedims(dW22)
		b[1] -= r.* dB21
		b[2] -= r.* dB22

		
		println(dW11)
		println(dW21)
		println(dW22)
		println()
		println(dB11)
		println(dB21)
		println(dB22)
		println()
		println(output)
		println(err)
		println("----")

	end
end


nn_create_and_train(2)
