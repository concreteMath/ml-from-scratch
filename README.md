# ML from scratch

Implementation of machine learning algorithms from scratch.

To really understand the algorithms, I have started from first principles, worked out the mathematics and code structure for each one of them.
The chosen programming language is Julia because it is fast ([see benchmarks](https://benchmarksgame-team.pages.debian.net/benchmarksgame/which-programs-are-fastest.html)) and has an easy syntax to express mathematical ideas.
