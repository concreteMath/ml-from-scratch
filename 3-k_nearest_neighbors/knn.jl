function fakedata(nGauss, nPoints)
	x = zeros(nGauss * nPoints, 2)
	y = zeros(nGauss * nPoints)
	for i=1:nGauss
		sg = rand() + 0.2
		x0 = randn(2)*4
		for j=1:nPoints
			k = (i-1)*nPoints + j
			x[k,:] = x0 + randn(2)*sg
			y[k] = i
		end
	end
	return x,y
end

function knnFit(x, y, nClusters)
	epsilon = 1e-6
	c = rand(nClusters, 2)
	c_old = zeros(nClusters, 2)
	c1 = zeros(nClusters, 2)
	c1_count = zeros(nClusters)
	N = size(x,1)
	class = zeros(size(x,1))
	for k=1:100
		c1 .= 0
		c1_count .= 0
		for i=1:N
			# Find closest center
			c_dist_min = Inf
			c_index = -1
			for j=1:nClusters
				d = sum((x[i,:] - c[j,:]).^2)
				if d < c_dist_min
					c_dist_min = d
					c_index = j
				end
			end
			c1[c_index,:] += x[i,:]
			c1_count[c_index] += 1
			class[i] = c_index
		end
		
		# Update dos centros
		for j=1:nClusters
			if c1_count[j] == 0
				continue
			end
			c[j,:] = c1[j,:] ./ c1_count[j]
		end

		# Break point
		dist_from_old = sqrt.(sum((c - c_old).^2, dims=2))
		if minimum(dist_from_old) < epsilon
			println(stderr, "k=$k")
			break
		end
		c_old = copy(c)
	end

	return c, class
end

function gnuplotPrint(x,class)
	for i=1:length(class)
		println("$(x[i,1])\t$(x[i,2])\t$(class[i])")
	end
end

nClusters = 4
x,y = fakedata(nClusters, 20)
c, class = knnFit(x,y, nClusters)

gnuplotPrint(x, class)
